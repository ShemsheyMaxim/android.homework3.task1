package com.maxim.timer;

import android.content.Context;
import android.icu.util.TimeUnit;
import android.os.Handler;
import android.os.Message;
import android.support.v4.util.TimeUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private EditText time;
    private Button start;
    private TextView tCountTime;
    int countTime = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        time = (EditText) findViewById(R.id.editText_time);
        start = (Button) findViewById(R.id.button_start);
        tCountTime = (TextView) findViewById(R.id.textView_count_time);

        final Handler handler = new Handler(){
            @Override
            public void handleMessage(Message msg) {
                countTime--;
                tCountTime.setText("" + countTime);
                if (msg.what == 1){
                    tCountTime.setText("Time go out!");
                    time.setVisibility(View.VISIBLE);
                    start.setVisibility(View.VISIBLE);
                }
            }

        };

        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(start.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

                String sTime = time.getText().toString();
                countTime = Integer.parseInt(sTime);

                v.setVisibility(View.GONE);
                time.setVisibility(View.GONE);
                tCountTime.setVisibility(View.VISIBLE);
                tCountTime.setText(sTime);

                final Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        for (int i = countTime; i >= 1; i-- ) {
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            handler.sendEmptyMessage(i);
                        }
                    }
                });
                thread.start();
            }
        });
    }
}
